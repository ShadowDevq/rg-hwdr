import numpy as np
import keras, os
from keras import layers
from PIL import Image

def resize_images(images):
    resized_images = []
    for image in images:
        img = Image.fromarray(image)
        img_resized = img.resize((44, 44))
        flattened_image = np.array(img_resized).flatten()
        resized_images.append(flattened_image)
    return np.array(resized_images)

def clean_data(data):
    (X_train, Y_train), (X_test, Y_test) = data

    X_train = resize_images(X_train)
    X_test = resize_images(X_test)

    X_train = X_train.astype('float32') / 255
    X_test = X_test.astype('float32') / 255

    Y_train = keras.utils.to_categorical(Y_train, 10)
    Y_test = keras.utils.to_categorical(Y_test, 10)

    return X_train, Y_train, X_test, Y_test

def train(model_name):
    X_train, Y_train, X_test, Y_test = clean_data(keras.datasets.mnist.load_data())

    seq = [keras.Input(shape=(1936,))]

    i=input("Number Of Hidden Layers (default=1): ")
    if i in [''," ",[],None,0] or int(i)<1: i=1
    layernum = int(i)

    for layer in range(layernum):
        i=input(f"Neurons in layer {layer} (default=10, min=10): ")
        if i in [''," ",[],None,0] or int(i)<10: i=10
        seq.append(layers.Dense(int(i), activation='relu'))
        
    seq.append(layers.Dense(10, activation='softmax'))
    
    model = keras.Sequential(seq)

    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    i=input("Batch Size (recommended=128): ")
    if i in [''," ",[],None,0] or int(i)<1: i=128
    j=input("Number Of Epochs (default=15): ")
    if j in [''," ",[],None,0] or int(j)<1: j=15
    model.fit(X_train, Y_train, batch_size=int(i), epochs=int(j), validation_split=0.1)
    model.save(model_name)

    loss_and_metrics = model.evaluate(X_test, Y_test, verbose=2)
    print("Test Loss", loss_and_metrics[0])
    print("Test Accuracy", loss_and_metrics[1])

def main():
    model_name = input("Model Filename: ")+'.keras'
    exist=os.path.exists(model_name)
    while 1:
        i=input("Train new model? (Y/N): ")
        if i.lower() == "y":
            trainq = True
            break
        elif i.lower() == "n":
            trainq = False
            break
    if trainq:
        train(model_name)
    try:model = keras.saving.load_model(model_name)
    except:
        print("Model file not found. Train model first.")
        exit()

    X_train, Y_train, X_test, Y_test = clean_data(keras.datasets.mnist.load_data())
    accuracy = model.evaluate(X_test, Y_test)[1]

    mdl=[
        input("Model Name: "),
        accuracy
    ]

    layers=[]
    for _layer in model.layers:
        layer = []
        wght = _layer.get_weights()[0]
        weights = [[] for i in range(len(wght[0]))]
        for weight in range(len(wght)):
            for w in range(len(wght[weight])):
                weights[w].append(wght[weight][w])
        layer.append(weights)
        layer.append(list(_layer.get_weights()[1]))
        layers.append(layer)
    
    mdl.append(layers)

    with open("data.lua","wt") as dataLUA:
        dataLUA.write("local models = {\n   "+str(mdl).replace("[","{").replace("]","}")+"\n}")
        dataLUA.close()

if __name__ == '__main__':
    main()