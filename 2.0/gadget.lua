-- MODELS --
local models = {
	-- Models here
}
-- Devices --
local vid = gdt.VideoChip0
local vidNum = gdt.VideoChip1
local lcd = gdt.Lcd0
local coinfidenceDigitLeds = {
	{gdt.Led0,gdt.Led1,gdt.Led2,gdt.Led3,gdt.Led4,gdt.Led5,gdt.Led6,gdt.Led7,gdt.Led8,gdt.Led9},
	{gdt.Led27,gdt.Led26,gdt.Led25,gdt.Led24,gdt.Led23,gdt.Led22,gdt.Led21,gdt.Led20,gdt.Led19,gdt.Led10},
	{gdt.Led36,gdt.Led35,gdt.Led34,gdt.Led33,gdt.Led32,gdt.Led31,gdt.Led30,gdt.Led29,gdt.Led28,gdt.Led11},
	{gdt.Led45,gdt.Led44,gdt.Led43,gdt.Led42,gdt.Led41,gdt.Led40,gdt.Led39,gdt.Led38,gdt.Led37,gdt.Led12},
	{gdt.Led54,gdt.Led53,gdt.Led52,gdt.Led51,gdt.Led50,gdt.Led49,gdt.Led48,gdt.Led47,gdt.Led46,gdt.Led13},
	{gdt.Led63,gdt.Led62,gdt.Led61,gdt.Led60,gdt.Led59,gdt.Led58,gdt.Led57,gdt.Led56,gdt.Led55,gdt.Led14},
	{gdt.Led72,gdt.Led71,gdt.Led70,gdt.Led69,gdt.Led68,gdt.Led67,gdt.Led66,gdt.Led65,gdt.Led64,gdt.Led15},
	{gdt.Led81,gdt.Led80,gdt.Led79,gdt.Led78,gdt.Led77,gdt.Led76,gdt.Led75,gdt.Led74,gdt.Led73,gdt.Led16},
	{gdt.Led90,gdt.Led89,gdt.Led88,gdt.Led87,gdt.Led86,gdt.Led85,gdt.Led84,gdt.Led83,gdt.Led82,gdt.Led17},
	{gdt.Led99,gdt.Led98,gdt.Led97,gdt.Led96,gdt.Led95,gdt.Led94,gdt.Led93,gdt.Led92,gdt.Led91,gdt.Led18}
}
local digitArt = {
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
}
-- Variables --
local modelSelected = 1
local width = (vid.Width/4)
local height = vid.Height/4
local size = width*height
local held = false
local lastPos = vec2(-1,-1)
local screen = {}
local enableProcessing = gdt.Switch0.State
for i=1,size do
	screen[i]=0
end
-- Functions --
function softmax(array:{number})
    local exp_sum = 0
    local result = {}
    for i = 1, #array do
        result[i] = math.exp(array[i])
        exp_sum = exp_sum + result[i]
    end
    for i = 1, #result do
        result[i] = result[i] / exp_sum
    end
    return result
end
function mul(array1:{number},array2:{number})
    local res = {}
    for i=1,#array1 do
        res[i]=array1[i]*array2[i]
    end
    return res
end
function gmp(pos:vec2)
    return (pos.Y*width)+pos.X+1
end
function line(pos1:vec2,pos2:vec2, brightness:number)
    local x0 = pos1.X
    local y0 = pos1.Y
    local dx = math.abs(pos2.X - x0)
    local dy = math.abs(pos2.Y - y0)
    local sx, sy
    if x0 < pos2.X then
        sx = 1
    else
        sx = -1
    end
    if y0 < pos2.Y then
        sy = 1
    else
        sy = -1
    end
    local err = dx - dy
    while true do
		if screen[gmp(vec2(x0,y0))] ~= nil then
        	screen[gmp(vec2(x0,y0))]=math.clamp(screen[gmp(vec2(x0,y0))]+brightness,0,1)
     	end   
		if x0 == pos2.X and y0 == pos2.Y then
            break
        end
        local e2 = 2 * err
        if e2 > -dy then
            err = err - dy
            x0 = x0 + sx
        end
        if e2 < dx then
            err = err + dx
            y0 = y0 + sy
        end
    end
end
function clearCoinfidences()
	for n=0,9 do
		for i=0,9 do
			coinfidenceDigitLeds[n+1][i+1].State = false
		end
	end
end
function renderConfidence(digit:number, confidence:number)
	if confidence ~= 0 then
		coinfidenceDigitLeds[digit+1][confidence].Color = Color(255,255,55)
		coinfidenceDigitLeds[digit+1][confidence].State = true
		for x=1,confidence-1 do
			coinfidenceDigitLeds[digit+1][x].Color = Color(55,55,0)
			coinfidenceDigitLeds[digit+1][x].State = true
		end
	end
end
function relu(array:{number})
	local r = {}
	for i=1,#array do
		r[i] = math.max(0,array[i])
	end
	return r
end
function renderConfidences(coinfidence:{number})
	local res = {}
  	local mn, mx = coinfidence[1], coinfidence[1]
  	for i = 2, #coinfidence do
  		if coinfidence[i] > mx then mx = coinfidence[i] end
    	if coinfidence[i] < mn then mn = coinfidence[i] end
  	end
  	local m = mx-mn
  	for n=1,#coinfidence do
    	res[n] = math.floor((9*(coinfidence[n]-mn))/m)
  	end
  	return res
end
function neuron(data:{number}, weights:{number}, bias:number)
    local r = 0
    local m = mul(data,weights)
    for x=1,#m do
        r=r+m[x]
    end
    return r+bias
end
function layer(layer:{number}, weights:{{number}}, biases:{number}, activation)
	local res = {}
    for i=1,#biases do
        res[i] = neuron(layer, weights[i], biases[i])
    end
	return activation(res)
end
function guess()
	local layers = models[modelSelected][3]
	local lastLayer = screen
	for i=1,#layers-1 do
		lastLayer = layer(lastLayer, layers[i][1], layers[i][2], relu)
	end
    return layer(lastLayer, layers[#layers][1], layers[#layers][2], softmax)
end
function maxKey(array:{number})
    local key, value = 1, array[1]
    for i = 2, #array do
        if value < array[i] then
            key, value = i, array[i]
        end
    end
    return key
end
function process()
	clearCoinfidences()
	local res = guess()
	local nr = digitArt[maxKey(res)]
	vidNum:Clear(color.black)
	for x=0,width do
		for y=0,height do
			local i = nr[((y*16)+x)+1]
			if i~= nil and i>0 then
				vidNum:SetPixel(vec2(x,y),color.white)
			end
		end
	end
	
	-- Displaying confidence --
	local probsn = renderConfidences(res)
	for i=1,#probsn do
		renderConfidence(i-1,probsn[i]+1)
	end
	render()
end
function render()
	vid:Clear(color.black)
	for x=0,width do
		for y=0,height do
			local i = screen[gmp(vec2(x,y))]
			if i~=nil and i>0 then
				for xi=1,4 do
					for yi=1,4 do
						vid:SetPixel(vec2((x*4)+xi,(y*4)+yi),Color(i*255,i*255,i*255))
					end
				end
			end
		end
	end
end
function renderLCD()
	local mdl = models[modelSelected]
	local text = mdl[1]
	for i=0,(15-#text) do
		text = text .. " "
	end
	text = text .. "Accuracy: " .. mdl[2]
	lcd.Text = text
end
-- Start --
renderLCD()
if enableProcessing then
	process()
else
	render()
end
-- Update --
function update()end
-- Event1 --
function eventChannel1(s:VideoChip, e:VideoChipTouchEvent)
	if e.TouchDown and not held then		
		held = true
		lastPos=vec2(
			math.floor(s.TouchPosition.X/4),
			math.floor(s.TouchPosition.Y/4)
		)
	elseif s.TouchPosition ~= vec2(-1,-1) then
		local pos = vec2(
			math.floor(s.TouchPosition.X/4),
			math.floor(s.TouchPosition.Y/4)
		)
		
		line(vec2(lastPos.X,lastPos.Y),
				 vec2(pos.X,pos.Y), 1)
				
		line(vec2(lastPos.X,lastPos.Y-1),
				 vec2(pos.X,pos.Y-1), 0.4)
		line(vec2(lastPos.X,lastPos.Y+1),
				 vec2(pos.X,pos.Y+1), 0.4)
		line(vec2(lastPos.X,lastPos.Y-2),
				 vec2(pos.X,pos.Y-2), 0.2)
		line(vec2(lastPos.X,lastPos.Y+2),
				 vec2(pos.X,pos.Y+2), 0.2)
				
		if pos.X ~= 0 then
			line(vec2(lastPos.X-1,lastPos.Y),
					 vec2(pos.X-1,pos.Y), 0.4)
			line(vec2(lastPos.X-1,lastPos.Y-1),
					 vec2(pos.X-1,pos.Y-1), 0.2)
			line(vec2(lastPos.X-1,lastPos.Y+1),
					 vec2(pos.X-1,pos.Y+1), 0.2)
		end
		
		if pos.X ~= 43 then
			line(vec2(lastPos.X+1,lastPos.Y),
					 vec2(pos.X+1,pos.Y), 0.4)
			line(vec2(lastPos.X+1,lastPos.Y+1),
					 vec2(pos.X+1,pos.Y+1), 0.2)
			line(vec2(lastPos.X+1,lastPos.Y-1),
					 vec2(pos.X+1,pos.Y-1), 0.2)
		end
		
		if (pos.X ~= 0) and (pos.X ~= 1) then
			line(vec2(lastPos.X-2,lastPos.Y),
					 vec2(pos.X-2,pos.Y), 0.2)
		end
		
		if (pos.X ~= 43) and (pos.X ~= 42) then
			line(vec2(lastPos.X+2,lastPos.Y),
					 vec2(pos.X+2,pos.Y), 0.2)
		end
		
		lastPos=pos
	else
		held = false
	end
	if enableProcessing then
		process()
	else
		render()
	end
end
-- Event2 --
function eventChannel2(s:LedButton, e:LedButtonEvent)
	for i=1,size do
    	screen[i] = 0
	end
	
	if enableProcessing then
		process()
	else
		render()
	end
end
-- Event3 --
function eventChannel3(s:LedButton, e:LedButtonEvent)
	if e.ButtonDown then
		modelSelected = math.clamp(modelSelected-1, 1,#models)
		renderLCD()
		
		if enableProcessing then
			process()
		else
			render()
		end
	end
end
-- Event4 --
function eventChannel4(s:LedButton, e:LedButtonEvent)
	if e.ButtonDown then
		modelSelected = math.clamp(modelSelected+1, 1,#models)
		renderLCD()
		if enableProcessing then
			process()
		else
			render()
		end
	end
end
-- Event5 --
function eventChannel5(s:Switch, e:SwitchStateChangeEvent)
	if e.State then
		process()
	end
	enableProcessing = e.State
end