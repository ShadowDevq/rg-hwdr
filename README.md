# Retro Gadgets Handwritten Digit Recognizer
**All code for HWDR V2 is in the 2.0 Folder**
## Links
**Steam:** https://steamcommunity.com/sharedfiles/filedetails/?id=3266289256

**Retro Gadgets Game:**  https://store.steampowered.com/app/1730260?snr=5000_5100___primarylinks
## Requirements
Python 3.9+ (Might also work with some older versions)
## How to Train Models
1. Open terminal in directory with main.py file
2. Run commands to install dependencies and run script
```bash
pip install -r requirements.txt
python main.py
```
3. Choose options
```python
# Name of file with model (you do not need to add .keras at the end)
Model Filename: model_filename
# You can either train a new model or use an existing one (Not all models are supported)
Train new model? (Y/N): Y
# Number of hidden layers of model
Number Of Нidden Layers (default=1): 1
# Number of neurons in each hidden layer
Neurons іn layer (?) (default=10, min=10): 10
# Batch Size (I don't recommend to change this one)
Batch Size (recommended=128): 128
# Number of times the model will be fed with data
Number Of Epochs (default=15): 15 
# Model name in-game (max length of 15 chars)
Model Name: TestModel
```
## How to import the model into the game
1. First you need to train a new model using the main.py script or using the same script use existing model (Not all models are supported)
2. After you run the script create a new lua file in your gadget assets in-game
3. Then open data.lua file and copy all data.lua file contents into a newly created lua file in-game
4. Open the CPU0.lua file in-game and add the name of the new file into it without the .lua ending to the model's list